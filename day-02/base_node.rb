
class BaseNode

  attr_accessor :vertices, :label


  def initialize
    @vertices = {}
    #@id = 0
  end

  def connect!(node, path)
    #create a node automatically
    @vertices[path] = node
    self
  end

  def break!(path)
    @vertices.delete(path)
    self
  end



  protected




end
