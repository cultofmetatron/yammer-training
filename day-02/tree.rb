
require_relative "base_node"

class Tree < BaseNode

  def initialize(data)
    super()
    @data = data
  end

  def set!(data)
    @data = data
    self
  end

  def set_value(data)
    self.clone.set!(data)
  end

  def value
    return @data
  end

  #made immutable
  def add_child(key, tree)
    self.clone.connect! tree, key
  end

  def get(key)
    self.vertices[key] #return the sub key
  end

  #reduces ambiguity in code
  def set_child(key, tree)
    self.add_child(key, tree)
  end

  #takes an array of keys and returns
  #the node at the end of that path
  def get_within(keys)
    keys.reduce self do |tree, key|
      tree.get(key)
    end
  end

  def set_within(keys, cb)
    #puts "set_within: #{keys}, #{cb.to_s}"
    if !keys.empty? || keys.nil?
      key = keys.shift #this modifies the keys array
      self.set_child(key, self.get(key).set_within(keys, cb))
    else
      return cb.call(self)
    end
  end

  def remove_child(key)
    self.clone.break! key
  end

  def map(callable)
    data = callable.call(self.value)
    initial_tree = self.class.new(data)
    vertices = {}
    self.vertices.each do |key, subtree|
      if subtree.respond_to?(:map)
        vertices[key] = subtree.map callable
      else
        nil
      end
    end
    initial_tree.vertices = vertices
    initial_tree
  end

  def clone
    self.map lambda {|identity| identity }
  end

  #returns a hash
  def to_h
    vertices = {}
    self.vertices.each do |key, tree|
      vertices[key] = tree.to_h
    end
    return { :data => @data.to_s(), :vertices => vertices }
  end

  def to_s
    return self.to_h.to_s
  end

end
