
require_relative "tree"

class LinkedList < Tree

  def initialize(data=nil)
    super(data)
    #self.vertices = { :next => nil }
  end

  def empty?
    self.value === nil && self.get(:next).nil?
  end

  #gives you the falue at the head of the list
  def head
    self.value
  end

  #gives you the rest of the list
  def tail
    self.vertices[:next]
  end

  #reusing map
  def map(callable)
    puts "calling llinked list map"
    if !self.empty? #prevents map on empty version
      super(callable)
    else
      self
    end
  end

  #oushes a value onto the head of the list and returns a new list
  def push(data)
    list = LinkedList.new(nil).set_value(data)
    list.add_child(:next, self)
  end

  #reduces over the list and returns a new list
  def reduce(memo, callable)

  end

  #returns a linked list of all the nodes that pass the callable
  #returns nil if empty
  def filter(callable)

  end

  def first(callable)

  end

  def intersection(lst)

  end

  def size

  end

  #starting from zero, return the value at the ith portion
  def at(index)

  end

  #at the index, slice off the index
  def slice(index)

  end

  #overide these to make it prettier!
  # def to_h
  #
  # end


end
