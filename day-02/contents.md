# Day 2 Notes

## Object Oriented programming

So far we have only worked with ruby functions. While You have solved some interesting problems with basic ruby, to build and architect larger, more robust solutions. It is imperative now to broach the topic of modelling using objects.

#### Method

A Method is a special kind of function that has some reference to a context. That context refers to the data pertinent to it within an object.

#### Object

An Object is a bundle of self contained data and methods that represent a unit of functionality. In ruby, everything is an object. This allows us to invoke functions contained within them.

```ruby

10.times do |n|
  puts n
end

# 0
# 1
# 2
# 3
# 4
# 5
# 6
# 7
# 8
# 9
# => 10

```

#### Class

A class is a way of specifiying a *template* for how to construct an object. Interestingly, classes themselves are objects in ruby and have their own methods!

Inside a class definition, we organize our code with respect to interfaces that perform actions. This may include modifying the internal data of the object or returning a new instance of the object. variables that persist within the lifecycle of the object are called class variables and are preceeded by a **@** symbol.

```ruby

#how to create a basic class
class Person
  #the initializer
  def initialize(name)
    @name = name
  end

  #The accessor for the 'name' class variable
  def name
    @name
  end

  #The setter for the 'name class variable'
  def name= name
    @name = name
  end

  #prints a greeting based on the name
  def greeting
    puts "hello, my name is #{@name}"
  end

end


#creating an object from a class
mark = Person.new('Mark')
mark.greeting
# hello, my name is peter
# => nil

```

#### private vs public data

Object oriented programming emphasizes management of complexity through the specification of interfaces through outside code interacts with the object. The main idea is that the implementation details should be opaque allowing the object to be used as a black box. A great example is 3 being a Fixnum and 999999999999999999 being a Bignum. FixNum and Bignum are kinds of integers and expose the interface of an integer. In the case of **3 + 4**, *+* is a method on 3. its equivalent to **3.add(4)**. The number, 99999999999999999 also has a method for addition. It doesn't matter that the way you add two Bignums is diffrent from adding two FixNums. The details of how the operation is performed is hidden through the Integer interface.

With that in mind, its considered best practice that all class variables be private and accessed via methods. To make this easier, ruby has sugar for specifying appropriate accessor and setter methods. Any method after the **private** declaration can only be accessed by other methods in the class.

``` ruby
#creates a setter and a getter
attr_accessor :name

#creates just an accesser
attr_reader :name

#specifies a writer
attr_writer :name

```

We can shorten up the original class above using this sugar.

```ruby
#how to create a basic class
class Person

  #this is equivlent to the name and name= methods defined above
  attr_accessor :name

  #the initializer
  def initialize(name)
    @name = name
  end

  #prints a greeting based on the name
  def greeting
    puts make_greeting
  end

  private

  def make_greeting
    "hello, my name is #{@name}"
  end

end

monsanto = Person.new('monsanto')
monsanto.greeting


```

#### Inheritance

Earlier we've created a Person class. In Object oriented programming, we can specify
that one class is a kind of another such that it inherits behavior. By default, all classes
inherit from [**BasicObject**](http://ruby-doc.org/core-2.2.0/BasicObject.html).

Now that we have a person, lets define a new subclass of person called Employee.

```ruby

class Employee < Person
  attr_accessor :id, :age
  def initialize(name, id, age, employer="monsanto")
    @name = name
    @id    = id
    @age   = age
    @employer = employer
  end

  def greeting
    super()
    puts "I work at #{@employer}"
  end

end

carl = Employee.new('Carl', 0, 30)
carl.greeting()

```
