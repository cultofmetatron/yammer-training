
require "rspec"
require_relative "../base_node.rb"
require_relative "../tree.rb"

RSpec.describe BaseNode do
  it "creates a new node" do
    node = BaseNode.new
  end


end


RSpec.describe Tree do
  it "a new tree" do
    #expect(2).to eq()
    new_tree = Tree.new("aTree")
    expect(new_tree.value()).to eq("aTree")

  end

  it "lets you add leaves!!" do
    #expect(2).to eq()
    new_tree = Tree.new(1)
    new_tree = new_tree
      .add_child('left', Tree.new(6))
      .add_child('right', Tree.new(20)
        .add_child('left', Tree.new(30)))


    new_tree = Tree.new(1)
      .add_child(:next, Tree.new(2)
        .add_child(:next, Tree.new(3)
          .add_child(:next, Tree.new(nil))))

    #puts new_tree.to_s
    #Excercise: make tests

  end

  it "lets you map over teh structure" do
    tree = Tree.new(1)
      .add_child('left', Tree.new(1))
      .add_child(
        'right',
        Tree.new(1)
          .add_child('left', Tree.new(1)))

    mapped_tree = tree.map lambda { |x| x + 1 }

    puts tree.to_s
    puts '#####################'
    puts mapped_tree.to_s
    #Excercise: make tests
  end

  it "lets you remove children" do
    tree1 = Tree.new(1)
      .add_child('left', Tree.new(1))
      .add_child('right', Tree.new(1))

    tree2 = tree1.remove_child 'left'

    #puts "remove #{tree2.to_s}"
    #Excercise: make tests
  end

  it "should get within" do
    tree1 = Tree.new(1)
      .add_child('left', Tree.new(1))
      .add_child(
        'right', Tree.new(2)
          .add_child('left', Tree.new(3))).map lambda { |x| x + 1 }

    expect(tree1.get_within(['right', 'left']).value).to eq(4)
  end

  it "sets within the function" do
    tree1 = Tree.new(1)
      .add_child('left', Tree.new(1))
      .add_child(
        'right', Tree.new(1)
          .add_child('left', Tree.new(1))).map lambda { |x| x + 1 }

    tree2 = tree1.set_within(['right', 'left'], lambda { |tree|
      tree
        .set_value('foobar')
        .add_child('kumar', Tree.new('peterson'))
    })

    #puts tree2.get_within(['right', 'left', 'kumar']).value
    expect(tree2.get_within(['right', 'left', 'kumar']).value).to eq('peterson')
  end

end
