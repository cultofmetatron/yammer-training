# Day 04 web programming with sinatra

Today we are going to be building out our first web application
using Sinatra + sqlite.


# The lifecycle of an http call

**server**: a computer in a network listening for connections requests

**client**: a computer in a network making the connection request

### http requests

The browser initiates an http call the server. On connection, a handshake
is initiated
