
require 'yaml'
require 'json'

module Loaders
  def load_data_file(file_name)
    data = ''
    File.open(file_name, "r") do |f|
      f.each_line do |line|
        data = data + line
      end
    end
    yield(data)
  end


  def load_config(file_name)
    load_data_file file_name do |file_data|
      YAML.load(file_data)
    end
  end


  def load_json(file_name)
    load_data_file file_name do |file_data|
      JSON.parse(file_data)
    end
  end

end
