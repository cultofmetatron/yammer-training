# Itinerary

welcome to day one!!


### Day One
* Philosophy and Roadmap
  * irb repl
  * assignment
  * types
  * truthiness
  * if else
  * loops
  * arrays and maps
  * basic unit testing


### Day Two
* functional vs OO style programming
  * blocks and the yield keyword
  * procs and lambdas
  * Objects
    * instance methods vs class methods
