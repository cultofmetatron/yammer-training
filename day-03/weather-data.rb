require_relative './utils'
require 'net/http'
require 'json'

#http://ruby-doc.org/stdlib-2.2.3/libdoc/net/http/rdoc/Net/HTTP.html
#base wrapper for http://www.openweathermap.com/

class OpenWeatherMap

  attr_reader :cities
  attr_reader :base_url

  def initialize(api_key="9c628484d7c38ae22fb4cc61e2ed7df2")
    @api_key = api_key
    @base_url = "http://api.openweathermap.org/data/2.5/"
    @cities = load_json("./us_cities.json")['cities']
  end

  def find_city_by_id(id)
    cities = self.cities.select do |city|
      city._id === id
    end
    cities[0]
  end

  def find_cities_by_name(name)
    city = self.cities.select do |city|
      city['name'] == name
    end
    city
  end

  def get_weather_by_city_name(name)
    endpoint = URI self.generate_url "weather?q=#{name}"
    JSON.parse(Net::HTTP.get(endpoint)) #returns the endpoint

  end

  def generate_url(resource)
    @base_url + resource + "&APPID=#{@api_key}"
  end


end
