require_relative "./utils"
require "sqlite3"

config = load_config "./config.yml"

db = SQLite3::Database.new config['database']
