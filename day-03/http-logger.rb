
require 'sinatra'
require 'yaml'
require 'json'
require_relative "utils"


config_data = load_config "./config.yml"

set :port, config_data['port'] #sets the port

#http://www.sinatrarb.com/intro.html
get '/' do
  '<h1>hello world</h1\>'
end

#basic function for extracting middleware
def parse_body(body)
  data = ''
  #http://ruby-doc.org/stdlib-1.9.3/libdoc/stringio/rdoc/StringIO.html#method-i-each_line
  body.each_line do |line|
    data = data + line
  end
  JSON.parse(data)
end

#example
post '/logger' do
  #get the posted data
  File.open(config_data['logfile'], 'a') do |file|
    file.write("**********Logged at #{Time.now}\n")
    file.write parse_body(request.body)
    file.write("\n")
    file.write("*****************************\n\n")
  end
  {
    :status => "success"
  }.to_json
end

post '/echo' do
    "fill me in"
end


initial_count = 0;
get '/count/add/:number' do
  initial_count = initial_count + params[:number].to_i
  "<h1>You added #{params[:number]} to get #{initial_count}"
end

get '/count' do
  "<h1>current count is #{initial_count}</h1>"
end


###############  Excercise  #####################


## basic user login

users = {} #store your users here
#takes a json username and password and stores it in the users has
post '/users' do

end

#takes a username and password and returns { "success": true } if it was successful
post '/login' do


end

# rock paper scissors!
wins = 0


get '/rpc/score' do
  "</h1>you have scored #{wins} times!!</h1>"
end

#rock paper scissors, pretty open ended, lets demo this out!!
post '/rpc/play' do
  puts request.body #here's a clue
  "fill me in"
end
