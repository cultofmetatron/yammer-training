require_relative "./utils"
require 'json'
#we load in the list of cities and feed them into the us_cities.json

File.open('./us_cities.json', 'a') do |out_file|
  cities = []
  File.open('./city.list.json', 'r') do |in_file|
    in_file.each_line do |line|
      #puts line
      parsed_line = JSON.parse line
      if parsed_line['country'] == 'US'
        cities.push(parsed_line)
      end
    end
  end
  out_file.write({
    :cities => cities
  }.to_json)
end
