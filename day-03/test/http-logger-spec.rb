require 'rspec'
require_relative "../http-logger"
require 'rack/test'
require_relative '../utils'

ENV['RACK_ENV'] = 'test'

#http://ruby-doc.org/stdlib-2.2.3/libdoc/net/http/rdoc/Net/HTTP.html
#https://github.com/brynary/rack-test/blob/master/lib/rack/test.rb

config_data = load_config "./config.yml"

RSpec.describe "http-logger" do

  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  it "returns a page with hello world" do
    get '/'
    expect(last_response).to be_ok
    expect(last_response.body).to eq('<h1>hello world</h1\>')
  end

  xit "returns whatever I post to it" do
    post '/echo', { :foo => "bar" }.to_json(), "CONTENT_TYPE" => "application/json"
    expect(last_response).to be_ok
    expect(JSON.parse(last_response.body)['foo']).to eq("bar")
  end





end
