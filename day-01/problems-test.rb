#stuff
require_relative "problems"
require "test/unit"

class TestProblems < Test::Unit::TestCase
  def test_add
    assert_equal(4, add(2, 2))
  end

  def test_word_count
    assert_equal(1, word_count("ruby"))
    assert_equal(3, word_count("I love ruby"))
    assert_equal(5, word_count("I love ruby on rails"))
  end

  def xtest_smallest_word
    assert_equal("I", smallest_word("I love apple pi"))
    assert_equal("tree", smallest_word("tree trunks"))
    assert_equal("am", smallest_word("am be"))
  end

  def xtest_longest_word
    assert_equal("apple", longest_word("I love apple pi"))
    assert_equal("trunks", longest_word("tree trunks"))
    assert_equal("am", longest_word("am be"))
  end

  def xtest_sub_string?
    assert_equal(true,  sub_string?("gaba", "ashfgabaasfdh"))
    assert_equal(true,  sub_string?("fru", "fructose"))
    assert_equal(true,  sub_string?("combination", "linearcombination"))
    assert_equal(false, sub_string?("glicky", "flicker"))
  end

  def xtest_is_annagram
    assert_equal(true, is_annagram?("George Bush", "He bugs Gore"))
    assert_equal(true, is_annagram?("Chairman Mao", "I am on a march"))
    assert_equal(true, is_annagram?("Emperor Octavian", "Captain over Rome"))
    assert_equal(true, is_annagram?("Elvis", "Lives"))
    assert_equal(false, is_annagram?("radium", "foobar"))
  end

  def xtest_vowel_count
    assert_equal(5, vowel_count("aeiou"))
    assert_equal(2, vowel_count("hello"))
    assert_equal(4, vowel_count("good morning"))
  end

  def xtest_mean_average
    assert_equal(2.5, mean_average([1, 2, 3, 4]))
    assert_equal(4, mean_average(2, 4, 3, 7))
    assert_equal(70, mean_average(50, 50, 30, 150))
    assert_equal(20, mean_average(20, 10, 30))
  end


  def xtest_num_characters
    assert_equal(1, num_characters('h', "hello"))
    assert_equal(2, num_characters('k', "kkfoo"))
    assert_equal(2, num_characters('e', "I like to tweet things"))
  end

  def xtest_reverse
    assert_equal("olleh", reverse("hello"))
    assert_equal("dcba", reverse("abcd"))
  end

  def xtest_is_palindrome?
    assert_equal(true,  is_palindrome?("radar"))
    assert_equal(false, is_palindrome?("foobar"))
  end

  def xtest_factorial
    assert_equal(1, factorial(1))
    assert_equal(1*2, factorial(2))
    assert_equal(1*2*3, factorial(3))
    assert_equal(1*2*3*4*5*6*7*8*9*10, factorial(10))
  end

  def xtest_fibonacci
    assert_equal(0, fibonacci(0))
    assert_equal(1, fibonacci(1))
    assert_equal(1, fibonacci(2))
    assert_equal(5, fibonacci(5))
    assert_equal(610, fibonacci(15))
    assert_equal(6765, fibonacci(20))
  end

  def xtest_merge
    array1 = [1, 5, 9]
    array2 = [2, 3, 4, 5, 8]
    merged = merge(array1, array2)
    assert_equal(true, merged.instance_of?(Array))
    assert_equal( 1, merged[0])
    assert_equal( 2, merged[1])
    assert_equal( 3, merged[2])
    assert_equal( 4, merged[3])
    assert_equal( 5, merged[4])
    assert_equal( 5, merged[5])
    assert_equal( 8, merged[6])
    assert_equal( 9, merged[7])
  end

  def test_merge_sort
    raise "impliment the test"
  end

  def test_gcd
    raise "impliment the test"
  end

  def permutations
    raise "impliment the test"
  end


end
