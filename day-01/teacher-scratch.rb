
def less_than_five(num)
  num < 5
end

def foobuz(num)
  if num < 5
    "less than 5"
  elsif (num >= 5) && (num < 10)
    "between 5 and less than 10"
  else
    "greater than 10"
  end
end

#"abcd" => [abcd bcd cd d]

# irb(main):002:0> print_substrings "hellothere"
# hellothere
# ellothere
# llothere
# lothere
# othere
# there
# here
# ere
# re
# e
# => nil
def print_substrings(n)
  #basebase
  if n.length == 1
    puts n
  else
    puts n
    print_substrings(n.slice(1..-1))
    puts "back: #{n}"
  end
end
