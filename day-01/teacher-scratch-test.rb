require_relative "teacher-scratch"
require "test/unit"

class TestScratch < Test::Unit::TestCase
  def test_foobuz
    assert_equal(foobuz(5), "between 5 and less than 10")
  end

end
