# Day 1 Notes


### Introduction

Welcome to the first day of our exploration into introductory programming with ruby.


### Assignment

To assign variables in ruby, simply come up with a new variable name and assign a value to it.

```ruby

aString = "hello"
aNumber = 5
anotherNumber = 6
puts aNumber + anotherNumber

```

### Basic Types

Ruby is a strong and dynamically typed language. Its also important to not that in Ruby, all types are objects. This has its own implications which we'll cover on day 2. Suffice to say, it means that numbers and strings can have their own methods.

In this case, 3 is an *instance* of **Fixnum** which implements the abstract class **Integer**. This means it has access to all the instance methods of **Integer** which you can find on its RDOC.

* [Integer](http://ruby-doc.org/core-2.2.0/Integer.html)
* [Fixnum](http://ruby-doc.org/core-2.2.0/Integer.html)

```ruby
3.class #=> Fixnum
3.instance_of? Fixnum #=> true

#this is an example of a do block, I'll cover
3.times do |num|
  puts num + 1 #outputs 1, 2, 3
end

puts 3.even? # outputs 3

```

#### [String](http://ruby-doc.org/core-2.2.0/String.html)

Strings are another important type. the string "hello" implements **String**

```ruby
"hello".class #=> String
"hello".capitalize    #=> "Hello"

"the quick brown fox jumped over the mangy dog".split('')
=> ["t", "h", "e", " ", "q", "u", "i", "c", "k", " ", "b", "r", "o", "w", "n", " ", "f", "o", "x", " ", "j", "u", "m", "p", "e", "d", " ", "o", "v", "e", "r", " ", "t", "h", "e", " ", "m", "a", "n", "g", "y", " ", "d", "o", "g"]

"the quick brown fox jumped over the mangy dog".split(' ').join('-')
=> "the-quick-brown-fox-jumped-over-the-mangy-dog"

```



#### [Symbol](http://ruby-doc.org/core-2.2.0/Symbol.html)

Symbols are reference values that can be used in place of strings. Since Symbols are stored as references, its much faster for the ruby interpreter to compare symbols rather than strings.

```ruby

hello = :hello

```


### Complex Types: Arrays and Hashes

#### [Array](http://ruby-doc.org/core-2.2.0/Array.html)

Arrays can hold more than one item.

```ruby

emptyArray = []
example = ["hello", :hello,  1, 3]
puts example[0] #> "hello"
example.push("another value")
puts example #=> ["hello", :hello, 1, 3, "another value"]

```

Useful for slicing arrays.

#### [Range](http://ruby-doc.org/core-2.2.0/Range.html)

```ruby
('a'..'e').to_a    #=> ["a", "b", "c", "d", "e"]
('a'...'e').to_a   #=> ["a", "b", "c", "d"]
```

#### [Hash](http://ruby-doc.org/core-1.9.3/Hash.html)

A hash is a key balue store. The keys can be strings, numbers or keywords.

```ruby
#creating a basic hash
aHash = {}
aHash[:keyword] = :value
aHash["keyword"] = "value"
aHash[3] = "a number key"
puts aHash
#=> {:keyword=>:value, "keyword"=>"value", 3=>"a number key"}

```

### Truthiness

Most programming langugaes have a concept of truthiness.
There are the basic boolean types *true* and *false* In ruby, numbers, Hashes, Arrays and Strings are all truthy.

There is a value called nil that is falsy.

You can negate a value with the *!* operator.


### Control Structures

#### if ... else ...

```ruby

if (*cond*)
  do_stuff()
end

#with an else clause
if (*cond*)
  do_stuff()
else
  do_other_stuff()
end

#multiple ifs
if (*cond*)
  do_stuff()
elsif (*cond2*)
  do_other_stuff()
else
  otherwiseDo()
end

```

#### unless ...

unless works like if only in reverse.

```ruby

unless (*cond*)
  ...
end

```


### loops

#### while

```ruby
i = 0
while (i < 10) do
  puts i
  i = i + 1
end

```

#### until

```ruby
i = 0
until (i == 10) do
  puts i
  i = i + 1
end

```

#### for

unlike while and until, for operates on iteraterables.

```ruby

#arrays are iterables
for i in [0, 1, 2, 3, 4, 5, 6, 7 , 8, 9]
  puts i
end

for i in 0..9
  puts i
end

#excercise: are hashes iterable?

```
